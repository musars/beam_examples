import apache_beam as beam

p1 = beam.Pipeline()
class_count = (
 
  p1
  | beam.io.ReadFromText('data/course_data.txt',skip_header_lines=1)

  | beam.Map(lambda record: record.split(','))
  | beam.Map(lambda record: (record[3], 1))
  | beam.CombinePerKey(sum)
 
  | beam.io.WriteToText('out/2_count_students_by_classes')
)

p1.run()

