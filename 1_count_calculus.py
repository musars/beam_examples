import apache_beam as beam

p1 = beam.Pipeline()
class_count = (
 
  p1
  | beam.io.ReadFromText('data/course_data.txt',skip_header_lines=1)

  | beam.Map(lambda record: record.split(','))
  | beam.Filter(lambda record: record[3] == 'calculus')
  | beam.Map(lambda record: (record[3], 1))
  | beam.CombinePerKey(sum)
 
  | beam.io.WriteToText('out/1_count_calculus_students')
)

p1.run()

