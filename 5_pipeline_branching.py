import apache_beam as beam


p1 = beam.Pipeline()


input_collection = (

    p1
    | "Read input data" >> beam.io.ReadFromText('data/course_data.txt', skip_header_lines=1)
    | "Split rows into columns" >> beam.Map(lambda record: record.split(','))
                  )

calculus_count = (
    input_collection
    | 'Filter calculus students' >> beam.Filter(lambda record: record[3] == 'calculus')
    | 'Pair each calc student with 1' >> beam.Map(lambda record: (record[3], 1))
    | 'Aggegate calculus'        >> beam.CombinePerKey(sum)
    )

physics_count = (
    input_collection
    | 'Filter physics students'  >> beam.Filter(lambda record: record[3] == 'physics')
    | 'Pair each phys student with 1' >> beam.Map(lambda record: (record[3], 1))
    | 'Aggregate physics' >> beam.CombinePerKey(sum)
    )

output =(
    (calculus_count,physics_count)
    | beam.Flatten()
    | beam.io.WriteToText('out/5_all_classes')
)

p1.run()