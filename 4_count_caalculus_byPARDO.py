import apache_beam as beam

class SplitRow(beam.DoFn):
  def process(self, element):
    return  [element.split(',')]

class FilterCalculusStudents(beam.DoFn):
  def process(self, element):
    if element[3] == 'calculus':
      return [element]
 
class PairStudents(beam.DoFn):
  def process(self, element):
    return [(element[3], 1)]

class Counting(beam.DoFn):
  def process(self, element):
    (key, values) = element
    return [(key, sum(values))]
 

p1 = beam.Pipeline()

student_counts = (
 
  p1
  | beam.io.ReadFromText('data/course_data.txt',skip_header_lines=1)
 
  | beam.ParDo(SplitRow())
  | beam.ParDo(FilterCalculusStudents())
  | beam.ParDo(PairStudents())
  | beam.GroupByKey()
  | beam.ParDo(Counting())
 
  | beam.io.WriteToText('out/4_count_students_by_classes_byPARDO')
)

p1.run()