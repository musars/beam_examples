from itertools import count
import apache_beam as beam

p1 = beam.Pipeline()
class_students = (
  p1
  | beam.io.ReadFromText('data/course_data.txt',skip_header_lines=1)

  | beam.Map(lambda record: record.split(','))
  | beam.Map(lambda record: (record[3], record[1]))
  | beam.GroupByKey()

  |beam.io.WriteToText('out/3_list_class_students')
)

p1.run()